<?php


define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->dirroot.'/course/lib.php');

// Ensure errors are well explained
set_debugging(DEBUG_DEVELOPER, true);

if (!is_enabled_auth('casdb')) {
    error_log('[AUTH CASDB] '.get_string('pluginnotenabled', 'auth_ldap'));
    die;
}

cli_problem('[AUTH CASDB] The sync users cron has been deprecated. Please use the scheduled task instead.');

// Abort execution of the CLI script if the auth_cas\task\sync_task is enabled.
$task = \core\task\manager::get_scheduled_task('auth_casdb\task\sync_task');
if (!$task->get_disabled()) {
    cli_error('[AUTH CASDB] The scheduled task sync_task is enabled, the cron execution has been aborted.');
}


$casauth = get_auth_plugin('casdb');
$casauth->sync_users(true);

