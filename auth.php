<?php
/**
 * CASDB设置：关闭多种登录方式
 * external database设置：打开该登录方式，并进行相应设置
 * @author hedaqing
 * 2015-11-16
 *
 */
defined('MOODLE_INTERNAL') || die();

// require_once($CFG->dirroot.'/auth/ldap/auth.php');
require_once($CFG->dirroot.'/auth/cas/CAS/CAS.php');
require_once($CFG->dirroot.'/auth/cas/auth.php');
/**
 * CASDB authentication plugin.
 */
class auth_plugin_casdb extends auth_plugin_cas {

    /**
     * Constructor.
     */
    function __construct() {
        global $CFG;
        require_once($CFG->libdir.'/adodb/adodb.inc.php');
        $this->authtype = 'casdb';
        $this->roleauth = 'auth_casdb';
        $this->errorlogtag = '[AUTH CASDB] ';
        $this->init_plugin($this->authtype);
        $this->authdbconfigged = 1;
        // 获取外部数据库登录的相关配置信息，通过配置external database的信息来获取
        $this->authdbconfig = get_config('auth/db');

        if(empty($this->authdbconfig)){
            $this->authdbconfigged = 0;
        }else{
            if (empty($this->authdbconfig->extencoding)) {
                $this->authdbconfig->extencoding = 'utf-8';
            }
        }
    }

    /**
     * Old syntax of class constructor for backward compatibility.
     */
    public function auth_plugin_casdb() {
        self::__construct();
    }

    /**
     * Connect to external database.
     * auth/db/auth.php/db_init
     * @return ADOConnection
     */
    function authdb_init() {
        // Connect to the external database (forcing new connection).
        $authdb = ADONewConnection($this->authdbconfig->type);
        if (!empty($this->authdbconfig->debugauthdb)) {
            $authdb->debug = true;
            ob_start(); //Start output buffer to allow later use of the page headers.
        }
        $authdb->Connect($this->authdbconfig->host, 
            $this->authdbconfig->user, 
            $this->authdbconfig->pass, 
            $this->authdbconfig->name, 
            true);
        $authdb->SetFetchMode(ADODB_FETCH_ASSOC);
        if (!empty($this->authdbconfig->setupsql)) {
            $authdb->Execute($this->authdbconfig->setupsql);
        }

        return $authdb;
    }
    /**
     * Add slashes, we can not use placeholders or system functions.
     * /auth/db/auth.php/ext_addslashes
     * @param string $text
     * @return string
     */
    function ext_addslashes($text) {
        if (empty($this->authdbconfig->sybasequoting)) {
            $text = str_replace('\\', '\\\\', $text);
            $text = str_replace(array('\'', '"', "\0"), array('\\\'', '\\"', '\\0'), $text);
        } else {
            $text = str_replace("'", "''", $text);
        }
        return $text;
    }

    /**
     * Authenticates user against CASDB
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     * @return bool Authentication success or failure.
     */
    function user_login ($username, $password) {
        // 用户auth字段为casdb时，首先进行db登录，如果失败，则进行cas登录
        if ( $this->authdbconfigged ) {
            if ( $this->externaluser_login($username, $password) ) { // 使用db进行登录验证
                return true;
            }
        }
        $this->connectCAS(); // 若db登录失败，则使用CAS登录
        return phpCAS::isAuthenticated() && (trim(core_text::strtolower(phpCAS::getUser())) == $username);
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function externaluser_login($username, $password) {
        global $CFG, $DB;

        $extusername = core_text::convert($username, 'utf-8', $this->authdbconfig->extencoding);
        $extpassword = core_text::convert($password, 'utf-8', $this->authdbconfig->extencoding);

        if ($this->is_internal()) {// 返回否,因为所有auth属性为cas的用户都不会在本地表中存密码，所以固定返回否。
            // Lookup username externally, but resolve
            // password locally -- to support backend that
            // don't track passwords.

            if (isset($this->authdbconfig->removeuser) and $this->authdbconfig->removeuser == AUTH_REMOVEUSER_KEEP) {
                // No need to connect to external database in this case because users are never removed and we verify password locally.
                if ($user = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id, 'auth'=>'db'))) {
                    return validate_internal_user_password($user, $password);
                } else {
                    return false;
                }
            }

            $authdb = $this->authdb_init();

            $rs = $authdb->Execute("SELECT *
                                      FROM {$this->authdbconfig->table}
                                     WHERE {$this->authdbconfig->fielduser} = '".$this->ext_addslashes($extusername)."'");
            if (!$rs) {
                $authdb->Close();
                debugging(get_string('auth_dbcantconnect','auth_db'));
                return false;
            }

            if (!$rs->EOF) {
                $rs->Close();
                $authdb->Close();
                // User exists externally - check username/password internally.
                if ($user = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id, 'auth'=>'db'))) {
                    return validate_internal_user_password($user, $password);
                }
            } else {
                $rs->Close();
                $authdb->Close();
                // User does not exist externally.
                return false;
            }

        } else {
            // Normal case: use external db for both usernames and passwords.

            $authdb = $this->authdb_init(); // 连接外部数据库

            if ($this->authdbconfig->passtype === 'md5') {   // Re-format password accordingly.
                $extpassword = md5($extpassword);
            } else if ($this->authdbconfig->passtype === 'sha1') {
                $extpassword = sha1($extpassword);
            }

            $rs = $authdb->Execute("SELECT *
                                      FROM {$this->authdbconfig->table}
                                     WHERE {$this->authdbconfig->fielduser} = '".$this->ext_addslashes($extusername)."'
                                           AND {$this->authdbconfig->fieldpass} = '".$this->ext_addslashes($extpassword)."'");
            if (!$rs) {
                $authdb->Close();
                debugging(get_string('auth_dbcantconnect','auth_db'));
                return false;
            }

            if (!$rs->EOF) {
                $rs->Close();
                $authdb->Close();
                return true;
            } else {
                $rs->Close();
                $authdb->Close();
                return false;
            }

        }
    }

     /**
     * Syncronizes users from LDAP server to moodle user table.
     *
     * If no LDAP servers are configured, simply return. Otherwise,
     * call parent class method to do the work.
     *
     * @param bool $do_updates will do pull in data updates from LDAP if relevant
     * @return nothing
     */
    function sync_users($do_updates=true) {
        if (empty($this->config->host_url)) {
            error_log('[AUTH CASDB] '.get_string('noldapserver', 'auth_cas'));
            return;
        }
        parent::sync_users($do_updates);
    }

    /**
     * Reads user information from LDAP and returns it as array()
     *
     * If no LDAP servers are configured, user information has to be
     * provided via other methods (CSV file, manually, etc.). Return
     * an empty array so existing user info is not lost. Otherwise,
     * calls parent class method to get user info.
     *
     * @param string $username username
     * @return mixed array with no magic quotes or false on error
     */
    function get_userinfo($username) {
        if (empty($this->config->host_url)) {
            return array();
        }
        return parent::get_userinfo($username);
    }

     /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

}
